# Teensy 3.x/4.0 CoreMark, Temperature and Power Benchmark

Tests made in November 19th 2019, under environment temperature 24.8 C, humidity 90%.

[CoreMark for Teensy](https://github.com/PaulStoffregen/CoreMark) 1.0, GCC5.4.1 20160919, Arduino IDE 1.8.10, 
Teensyduino 1.48, under Manjaro Linux.

`f` is the Teensy main clock frequency.

`T` is the temperature (in degree Celsius) of the MCU measured after >= 3 runnings in sequence.

Electrical current `i` in mA, measured with [USB meter UM25C](https://www.aliexpress.com/item/32855845265.html), (Vusb = 5.06-5.07 V), 
with Arduino IDE connected to USB serial (cable).

## [Teensy 4.0](https://www.pjrc.com/store/teensy40.html)

Temperature in Teensy 4.0 using the `tempmonGetTemp()` bultin function. See the Arduino sketch 
['CoreMark_GetTemp_T4.ino'](./CoreMark_GetTemp_T4.ino).

Using a black aluminium heatsink from Raspberry Pi in 1 Teensy 4.0. Another Teensy 4.0 without heatsink.

| CoreMark | f (MHz) | T (C) | T with heatsink (C) | i (mA) | CoreMark/mA | Comments |
|:--------:|:-------:|:-----:|:-------------------:|:------:|:-----------:|:---------|
|  115.43  |    24   | 34.0  |        33.2         |   35   |     3.3     |          |
|  582.81  |   150   | 37.6  |        35.7         |   51   |    11.4     |          |
|  1735.11 |   450   | 43.5  |        40.1         |   80   |    21.7     |          |
|  2035.97 |   528   | 44.7  |        40.8         |   84   |    24.2     | works at lower MCU voltage = 1.15V |
|  2313.57 |   600   | 48.9  |        43.3         |  105   |    22.0     | default frequency, MCU voltage = 1.25V |
|  2776.24 |   720   | 54.3  |        45.8         |  134   |    20.7     |          |
|  3146.39 |   816   | 60.3  |        50.2         |  166   |    19.0     |          |
|  3516.79 |   912   | 69.3  |        55.3         |  213   |    16.5     |          |
|  3701.88 |   960   | 72.9  |        58.4         |  233   |    15.9     |          |
|  3840.74 |  1008   | 75.3  |        60.3         |  249   |    15.4     | Teensy 4.0 without heatsink damaged ! |

**DO NOT USE Teensy 4.0 without heatsink with frequencies > 600 MHz**.  
My Teensy 4.0 without heatsink stopped working after 4 runnings of the last line of the above benchmark : Arduino IDE 
can flash any software, but it doesn't run.

## Teensy 3.x

Temperature in Teensy 3.x/LC using [Teensy Internal Temperature library](https://github.com/LAtimes2/InternalTemperature), 
which should be placed at `~/Arduino/libraries/InternalTemperature`, changing `#include <arduino.h>` with `#include <Arduino.h>` 
in "InternalTemperature.cpp".  
See the Arduino sketch ['CoreMark_GetTemp_T3.ino'](./CoreMark_GetTemp_T3.ino).

### [Teensy 3.6](https://www.pjrc.com/store/teensy36.html)

| CoreMark | f (MHz) | T (C) | i (mA) | CoreMark/mA | Comments |
|:--------:|:-------:|:-----:|:------:|:-----------:|:---------|
|    -     |     2   |    -  |   1.8  |      -      | without USB communication |
|    -     |     8   |    -  |    8   |      -      | without USB communication |
|    -     |    16   |    -  |   11   |      -      | without USB communication |
|   58.41  |    24   |  29.4 |   19   |     3.1     |          |
|  117.21  |    48   |  30.1 |   27   |     4.3     |          |
|  176.00  |    72   |  30.7 |   32   |     5.5     |          |
|  234.78  |    96   |  32.4 |   40   |     5.9     |          |
|  293.56  |   120   |  33.2 |   49   |     6.0     |          |
|  352.40  |   144   |  35.4 |   61   |     5.8     |          |
|  411.18  |   168   |  36.2 |   69   |     6.0     |          |
|  440.56  |   180   |  37.5 |   73   |     6.0     | default frequency |
|  528.69  |   216   |  39.6 |   83   |     6.4     |          |
|  587.51  |   240   |  40.0 |   92   |     6.4     |          |
|  626.67  |   256   |  40.9 |   97   |     6.5     |          |

### [Teensy 3.5](https://www.pjrc.com/store/teensy35.html)

| CoreMark | f (MHz) | T (C) | i (mA) | CoreMark/mA | Comments |
|:--------:|:---------------:|:-----:|:------:|:-----------:|:---------|
|    -     |     2   |    -  |   1.4  |      -      | without USB communication |
|    -     |     8   |    -  |    7   |      -      | without USB communication |
|    -     |    16   |    -  |   10   |      -      | without USB communication |
|   58.43  |    24   |  29.3 |   17   |     3.4     |          |
|  114.88  |    48   |  31.7 |   27   |     4.3     |          |
|  168.56  |    72   |  32.7 |   32   |     5.3     |          |
|  218.44  |    96   |  33.9 |   40   |     5.5     |          |
|  266.74  |   120   |  34.7 |   47   |     5.7     | default frequency |
|  320.20  |   144   |  35.3 |   53   |     6.0     |          |
|  362.45  |   168   |  36.5 |   60   |     6.0     |          |

### [Teensy 3.2](https://www.pjrc.com/store/teensy32.html)

| CoreMark | f (MHz) | T (C) | i (mA) | CoreMark/mA | Comments |
|:--------:|:---------------:|:-----:|:------:|:-----------:|:---------|
|    -     |     2   |    -  |    2   |      -      | without USB communication |
|    -     |     8   |    -  |    7   |      -      | without USB communication |
|    -     |    16   |    -  |   11   |      -      | without USB communication |
|   58.29  |    24   |  33.1 |   17   |     3.4     |           |
|  114.51  |    48   |  35.2 |   26   |     4.4     |           |
|  166.91  |    72   |  36.0 |   30   |     5.6     | default frequency |
|  215.58  |    96   |  37.1 |   38   |     5.7     |           |  
|  257.57  |   120   |  40.5 |   45   |     5.7     |           |

### [Teensy LC](https://www.pjrc.com/store/teensylc.html)

| CoreMark | f (MHz) | T (C) | i (mA) | CoreMark/mA | Comments |
|:--------:|:-------:|:-----:|:------:|:-----------:|:---------|
|  30.53   |    24   |  32.5 |    9   |     3.4     |          |
|  57.78   |    48   |  33.5 |   12   |     4.8     | default frequency |


## Analysis

- **Teensy 4.0 is very performant**, 3840.74 CoreMarks @ 1008 MHz, **6x the Teensy 3.6**;  
- **Teensy 4.0 is very calculation efficient** , with 24.2 CoreMark/mA @ 528 MHz, approx. **4-5x the Teensy 3.x/LC**;
- **Teensy 4.0 recommended frequency is 528 MHz** due to have the best calculation efficiency, lower temperature and power usage;
- Teensy 4.0 consumes a lot, up to 249 mA, 2.5x Teensy 3.6, 4.2x Teensy 3.5, 5.5x Teensy 3.2 and 21x Teensy LC;
- **Teensy 4.0 should use heatsink for clock frequency > 600 MHz**, e. g., with > 800 MHz there is a great risk of damaging 
the Teensy 4.0 MCU, with the heatsink the temperature is up to 15 C lower;
- Teensy LC is the lowest power Teensy with i = 9 mA at 24 MHz, while at this frequency Teensy 3.2/3.5/3.6 
consumes 17-19 ma and Teensy 4.0 consumes 35 mA.


## Other Teensy benchmarks

- ['coremarkish plot' by manitou](https://forum.pjrc.com/threads/54711-Teensy-4-0-First-Beta-Test?p=194245&viewfull=1#post194245);
- ['Teensy 3.6 vs 4.0: measured power draw at various MHz' y XFer](https://forum.pjrc.com/threads/60698-Teensy-3-6-vs-4-0-measured-power-draw-at-various-MHz)
## To Do

- plots of CoreMark x Frequency, i x Frequency, T x Frequency, etc;
- also use [Teensy Internal Temperature library](https://github.com/LAtimes2/InternalTemperature) for Teensy 4.0 
temperature, feature not available in 2019, so only one sketch is enough for Teensy 3.x/4.x;
- remake using newer Arduino IDE 1.8.12 and Teensyduino 1.52 (except damaging the Teensy 4.0 without heatsink...);
- try to use clock frequencies < 24 MHz for Teensy LC by modifying and compiling the source code, see 
[the thread 'LC: any way to get slower clocks as in T3.2?' in PJRC/Teensy Forum](https://forum.pjrc.com/threads/61190-LC-any-way-to-get-slower-clocks-as-in-T3-2);

